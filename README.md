
Letto, [03.02.21 18:18]
version: 2
jobs: # we now have four jobs, so that a workflow can coordinate them!
  design: # This is our first job.
    docker: # it uses the docker executor
      - image: circleci/ruby:2.4.1 # specifically, a docker image with ruby 2.4.1
        auth:
          username: mydockerhub-user
          password: $DOCKERHUB_PASSWORD  # context / project UI env-var reference
    # Steps are a list of commands to run inside the docker container above.
    steps:
      - checkout # this pulls code down from GitHub
      - run: echo "A first hello" # This prints "A first hello" to stdout.
      - run: sleep 9999999 # a command telling the job to "sleep" for 9999999 seconds.
  by: # This is our second job.
    docker: # it runs inside a docker image, the same as above.
      - image: circleci/ruby:2.4.1
        auth:
          username: mydockerhub-user
          password: $DOCKERHUB_PASSWORD  # context / project UI env-var reference
    steps:
      - checkout
      - run: echo "A more familiar hi" # We run a similar echo command to above.
      - run: sleep 9999999 # and then sleep for 9999999 seconds.
  andika: # This is our second job.
    docker: # it runs inside a docker image, the same as above.
      - image: circleci/ruby:2.4.1
        auth:
          username: mydockerhub-user
          password: $DOCKERHUB_PASSWORD  # context / project UI env-var reference
    steps:
      - checkout
      - run: echo "A more familiar hi" # We run a similar echo command to above.
      - run: sleep 9999999 # and then sleep for 9999999 seconds.
  candra: # This is our second job.
    docker: # it runs inside a docker image, the same as above.
      - image: circleci/ruby:2.4.1
        auth:
          username: mydockerhub-user
          password: $DOCKERHUB_PASSWORD  # context / project UI env-var reference
    steps:
      - checkout
      - run: echo "A more familiar hi" # We run a similar echo command to above.
      - run: sleep 9999999 # and then sleep for 9999999 seconds.
# Under the workflows: map, we can coordinate our ten jobs, defined above.
workflows:
  version: 2
  one_and_two: # this is the name of our workflow
    jobs: # and here we list the jobs we are going to run.
      - design
      - by
      - andika
      - candra